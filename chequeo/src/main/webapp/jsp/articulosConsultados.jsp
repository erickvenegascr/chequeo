<%-- 
    Document   : articulosConsultados
    Created on : 22/05/2017, 01:53:35 PM
    Author     : erick
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript">
            var TableBackgroundMouseoverColor = '#D2691E';

            // These two functions need no customization.
            function ChangeBackgroundColor(row) {
                row.style.backgroundColor = TableBackgroundMouseoverColor;
            }
            function RestoreBackgroundColor(row) {
                row.style.backgroundColor = '';
            }
        </script>
        <script type="text/javascript" src="static/js/jquery.js"></script>
        <script type="text/javascript" src="static/js/jquery.tablesorter.js"></script>
        <script type="text/javascript">
            $(document).ready(function ()
            {
                $("#precios").tablesorter();
            }
            );
        </script>
        <link href="static/css/styles.css" rel="stylesheet" type="text/css"/>
        <title>Precios...</title>
    </head>
    <body>
        <table id="precios">
            <thead> 
                <tr>
                    <th>Código</th>
                    <th>Nombre</th>
                    <th>Cantidad</th>
                    <th>Precio</th>
                </tr>
            </thead> 
            <tbody>
                <fmt:setLocale value = "en_US"/>
                <c:forEach items="${articulos}" var="articulo">
             
                    <c:choose>
                        <c:when test="${articulo.importado && articulo.activo}">
                            <tr class="articuloImportado" onmouseover="ChangeBackgroundColor(this)" onmouseout="RestoreBackgroundColor(this)">
                                <td><c:out value="${articulo.codigo}"></c:out></td>
                                <td class="nombre"><c:out value="${articulo.nombre}"></c:out></td>
                                <td class="numero"><fmt:formatNumber type="number" maxFractionDigits="2" value="${articulo.existencia}"/></td>
                                <td class="numero"><fmt:formatNumber pattern="###,###.##" type="number" minFractionDigits="2" maxFractionDigits="2"  value="${articulo.precio}"/></td>
                            </tr>
                        </c:when>      
                        <c:when test="${articulo.activo && articulo.articuloBajaRotacion}">
                             <tr class="articuloSinMovimiento" onmouseover="ChangeBackgroundColor(this)" onmouseout="RestoreBackgroundColor(this)">
                                <td><c:out value="${articulo.codigo}"></c:out></td>
                                <td class="nombre"><c:out value="${articulo.nombre}"></c:out></td>
                                <td class="numero"><fmt:formatNumber type="number" maxFractionDigits="2" value="${articulo.existencia}"/></td>
                                <td class="numero"><fmt:formatNumber pattern="###,###.##" type="number" minFractionDigits="2" maxFractionDigits="2"  value="${articulo.precio}"/></td>
                            </tr>
                        </c:when>
                         <c:when test="${articulo.activo}">
                             <tr  onmouseover="ChangeBackgroundColor(this)" onmouseout="RestoreBackgroundColor(this)">
                                <td><c:out value="${articulo.codigo}"></c:out></td>
                                <td class="nombre"><c:out value="${articulo.nombre}"></c:out></td>
                                <td class="numero"><fmt:formatNumber type="number" maxFractionDigits="2" value="${articulo.existencia}"/></td>
                                <td class="numero"><fmt:formatNumber pattern="###,###.##" type="number" minFractionDigits="2" maxFractionDigits="2"  value="${articulo.precio}"/></td>
                            </tr>
                        </c:when>
                        <c:otherwise>
                            <tr class="articuloInactivo" onmouseover="ChangeBackgroundColor(this)" onmouseout="RestoreBackgroundColor(this)" class="articuloInactivo">
                                <td><c:out value="${articulo.codigo}"></c:out></td>
                                <td class="nombre"><c:out value="${articulo.nombre}"></c:out></td>
                                <td class="numero"><fmt:formatNumber type="number" maxFractionDigits="2" value="${articulo.existencia}"/></td>
                                <td class="numero"><fmt:formatNumber pattern="###,###.##" type="number" minFractionDigits="2" maxFractionDigits="2"  value="${articulo.precio}"/></td>
                            </tr>                                
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </tbody> 
        </table>
    </body>
</html>
