<%-- 
    Document   : articulosConsultados
    Created on : 22/05/2017, 01:53:35 PM
    Author     : erick
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript">
            var TableBackgroundMouseoverColor = '#D2691E';

            // These two functions need no customization.
            function ChangeBackgroundColor(row) {
                row.style.backgroundColor = TableBackgroundMouseoverColor;
            }
            function RestoreBackgroundColor(row) {
                row.style.backgroundColor = '';
            }
             
            
        </script>
        <link href="static/css/styles.css" rel="stylesheet" type="text/css"/>
        <title>Chequeo</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
        

    </head>
    <body>

        <fmt:setLocale value = "en_US"/>
            <div class="header">
                    <form action="/chequeo">
                        <input type="submit" value="Lista de Chequeos"/>
                    </form>
                    Factura: <c:out value="${factura.factura}"></c:out> </br>
                    <a href="/chequeo/?accion=chequeosfactura&factura=<c:out value="${factura.factura}"></c:out>">Chequeos</a></br>
                    Cliente: <c:out value="${factura.cliente}"></c:out> <c:out value="${factura.clienteNombre}"></c:out></br>
                    Fecha: <fmt:formatDate value="${factura.fecha}" pattern="dd-MM-yy hh:mm:ss a"/></br>

            </div>
            <c:forEach items="${factura.articulos}" var="a">
               <div class="col-3 right">
                   <div class="aside">
                       <h2> <c:out value="${a.nombre}"></c:out></h2>
                     Cod: <c:out value="${a.articulo}"></c:out>
                    Facturado: <c:out value="${a.cantidad}"></c:out>
                     </div>
                    </div>
            </c:forEach>

    </body>
</html>
