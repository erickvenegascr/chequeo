<%-- 
    Document   : articulosConsultados
    Created on : 22/05/2017, 01:53:35 PM
    Author     : erick
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript">
            var TableBackgroundMouseoverColor = '#D2691E';

            // These two functions need no customization.
            function ChangeBackgroundColor(row) {
                row.style.backgroundColor = TableBackgroundMouseoverColor;
            }
            function RestoreBackgroundColor(row) {
                row.style.backgroundColor = '';
            }
            setTimeout('document.location.reload()', 120000);
        </script>
        <script type="text/javascript" src="static/js/jquery.js"></script>
        <script type="text/javascript" src="static/js/jquery.tablesorter.js"></script>
        <script type="text/javascript">
            $(document).ready(function ()
            {
                $("#chequeo").tablesorter();
            }
            );
        </script>
        <link href="static/css/styles.css" rel="stylesheet" type="text/css"/>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
        <title>Chequeo</title>
    </head>
    <body>

        <jsp:useBean id="now" class="java.util.Date" />
        Refrescado: <fmt:formatDate value="${now}" pattern="dd-MM-yy hh:mm:ss a"/>
        <a href="/chequeo?accion=masDeUnaChequeada">Facturas chequeadas varias vences</a>


            <form action="/chequeo" class="flotante">
                <input type="submit" value="LISTA CHEQUEOS"/>
            </form>

            <table id="chequeo">
                <thead> 
                    <tr>
                        <th>#</th>
                        <th>Factura</th>
                        <th>Usuario</th>
                        <th>Fecha</th>
                        <th>Estacion</th>
                    </tr>
                </thead> 
                <tbody>
                <fmt:setLocale value = "en_US"/>

                <c:forEach items="${chequeos}" var="chequeo">

                    <c:choose>
                        <c:when test="${chequeo.cantChequeo>1}">
                            <tr class="atencion" onmouseover="ChangeBackgroundColor(this)" onmouseout="RestoreBackgroundColor(this)">
                                <td><c:out value="${chequeo.chequeo}"></c:out></td>
                                <td class="texto"><a href="/chequeo/?accion=consultarDetalleFactura&factura=<c:out value="${chequeo.factura}"></c:out>"><c:out value="${chequeo.factura}"></c:out></a></td>
                                <td class="texto"><c:out value="${chequeo.usuarioChequeo}"></c:out></td>
                                <td><fmt:formatDate value="${chequeo.fechaChequeo}" pattern="dd-MM-yy hh:mm:ss a"/></td>
                                <td class="texto"><c:out value="${chequeo.estacion}"></c:out></td>
                            </tr>

                        </c:when>
                        <c:otherwise>
                            <tr onmouseover="ChangeBackgroundColor(this)" onmouseout="RestoreBackgroundColor(this)">
                                <td><c:out value="${chequeo.chequeo}"></c:out></td>
                                <td class="texto"><a href="/chequeo/?accion=consultarDetalleFactura&factura=<c:out value="${chequeo.factura}"></c:out>"><c:out value="${chequeo.factura}"></c:out></a></td>
                                <td class="texto"><c:out value="${chequeo.usuarioChequeo}"></c:out></td>
                                <td><fmt:formatDate value="${chequeo.fechaChequeo}" pattern="dd-MM-yy hh:mm:ss a"/></td>
                                <td class="texto"><c:out value="${chequeo.estacion}"></c:out></td>
                            </tr>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </tbody> 
        </table>
    </body>
</html>
