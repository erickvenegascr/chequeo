<%-- 
    Document   : index.jsp
    Created on : 19/05/2017, 10:17:34 AM
    Author     : erick
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://www.w3schools.com/lib/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open Sans">
        <style>
            h1,h2,h3,h4,h5,h6 {font-family: "Oswald"}
            body {font-family: "Open Sans"}
        </style>
        <title>Precio</title>
    </head>
    <body>
        <form action="http://192.168.0.27:8080/pentaho/api/repos/%3Apublic%3AFacturasDespachoPendiente.prpt/viewer?" method="post">
            <input type="hidden" name="userid" id="userid" value="admin"/>
            <input type="hidden" name="password" id="password" value="password"/>
            <input type="submit" value="Facturas POS">
        </form>
        <form action="http://192.168.0.27:8080/pentaho/api/repos/%3Apublic%3AInventarioFisicoProveedor.prpt/viewer?" method="post">
            <input type="hidden" name="userid" id="userid" value="admin"/>
            <input type="hidden" name="password" id="password" value="password"/>
            <input type="submit" value="Fisico">
        </form>
        <form name="consulta" method="get">
            <input type="hidden" name="accion" value="consultar"/>
            <table border="1">
                <tbody>
                    <tr>
                        <td>Descripción: </td>
                        <td><input type="text" name="descripcion" value="" size="20" /></td>
                    </tr>
                    <tr>
                        <td>Código Artículo </td>
                        <td><input type="text" name="codProducto" value="" size="20" /></td>
                    </tr>
                </tbody>
            </table>
            <div>

                <input type="submit" value="consultar">
            </div>
        </form>
    </body>
</html>
