<%-- 
    Document   : noencontrado
    Created on : 25/05/2017, 03:19:15 PM
    Author     : erick
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://www.w3schools.com/lib/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open Sans">
        <style>
            h1,h2,h3,h4,h5,h6 {font-family: "Oswald"}
            body {font-family: "Open Sans"}
            table {
                border-collapse: collapse;
                width: 100%;
            }

            th, td {
                text-align: center;
                padding: 8px;
            }

            td.nombre{
                text-align: left;
                padding: 8px;
            }

            td.numero{
                text-align: right;
                padding: 8px;
            }

            tr:nth-child(even){background-color: #f2f2f2}

            th {
                background-color: #4CAF50;
                color: white;
            }
        </style>
        <title>No encontrado</title>
        <script type="text/javascript">
        </script>
    </head>
    <body>
        <div align="center">
            <h1>No se encontraron articulos<br>
                con la información suministrada.
            </h1>
            <button type="button" onclick="history.back();">Retornar</button>
        </div>
    </body>
</html>
