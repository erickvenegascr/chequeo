package com.almacen3r.chequeo;



import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author erick
 */
public class Chequeo {

    private int chequeo;
    private String factura;
    private String usuarioChequeo;
    private Timestamp fechaChequeo;
    private Boolean anulado;
    private String razonAnulado;
    private String estacion;
    private int cantChequeo;

    public Chequeo() {

    }

    public Chequeo(int chequeo, String factura, String usuarioChequeo, Timestamp fechaChequeo, boolean anulado, String razonAnulado, int cantChequeo, String estacion) {
        this.chequeo = chequeo;
        this.factura = factura;
        this.usuarioChequeo = usuarioChequeo;
        this.fechaChequeo = fechaChequeo;
        this.anulado = anulado;
        this.razonAnulado = razonAnulado;
        this.cantChequeo = cantChequeo;
        this.estacion = estacion;
    }

    public int getChequeo() {
        return chequeo;
    }

    public void setChequeo(int chequeo) {
        this.chequeo = chequeo;
    }

    public String getFactura() {
        return factura;
    }

    public void setFactura(String factura) {
        this.factura = factura;
    }

    public String getUsuarioChequeo() {
        return usuarioChequeo;
    }

    public void setUsuarioChequeo(String usuarioChequeo) {
        this.usuarioChequeo = usuarioChequeo;
    }

    public Timestamp getFechaChequeo() {
        return fechaChequeo;
    }

    public void setFechaChequeo(Timestamp fechaChequeo) {
        this.fechaChequeo = fechaChequeo;
    }

    public Boolean getAnulado() {
        return anulado;
    }

    public void setAnulado(Boolean anulado) {
        this.anulado = anulado;
    }

    public String getRazonAnulado() {
        return razonAnulado;
    }

    public void setRazonAnulado(String razonAnulado) {
        this.razonAnulado = razonAnulado;
    }

    public int getCantChequeo() {
        return cantChequeo;
    }

    public void setCantChequeo(int cantChequeo) {
        this.cantChequeo = cantChequeo;
    }

    public String getEstacion() {
        return estacion;
    }

    public void setEstacion(String estacion) {
        this.estacion = estacion;
    }

    @Override
    public String toString() {
        return "Chequeo{" + "chequeo=" + chequeo + ", factura=" + factura + ", usuarioChequeo=" + usuarioChequeo + ", fechaChequeo=" + fechaChequeo + ", anulado=" + anulado + ", razonAnulado=" + razonAnulado + ", estacion=" + estacion + ", cantChequeo=" + cantChequeo + '}';
    }

}
