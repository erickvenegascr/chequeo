package com.almacen3r.chequeo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import com.almacen3r.chequeo.Chequeo;

/**
 *
 * @author erick
 */
public class ChequeoDAO {

    private Connection con;

    public ChequeoDAO(Connection con) {
        this.con = con;
    }

    public ArrayList<Chequeo> getChequeos(Date fecha) throws SQLException {
        ArrayList<Chequeo> chequeos = new ArrayList<>();
        String sql = "SELECT Chequeo\n"
                + "      ,Factura\n"
                + "      ,UsuarioChequeo\n"
                + "      ,FechaChequeo\n"
                + "      ,Anulado\n"
                + "      ,RazonAnulado\n "
                + "      ,(SELECT COUNT(FACTURA) FROM [DM].[dbo].[Chequeo] CUENTA WHERE CUENTA.Factura=C.Factura) CantChequeo "
                + "      ,Estacion "
                + "  FROM DM.dbo.Chequeo C\n"
                + " WHERE\n"
                + "	C.FechaChequeo>=CAST(GETDATE() as Date)" 
                + "ORDER BY\n"
                + "	FechaChequeo DESC";
        PreparedStatement st= this.con.prepareStatement(sql);
        st.setDate(1, (java.sql.Date) fecha);
        ResultSet rs=st.executeQuery();
        while(rs.next()){
            chequeos.add(
                    new Chequeo(rs.getInt("Chequeo"),
                            rs.getString("Factura"),
                            rs.getString("UsuarioChequeo"),
                            rs.getTimestamp("FechaChequeo"),
                            0==rs.getInt("Anulado")?false:true,
                            rs.getString("RazonAnulado"),
                            rs.getInt("CantChequeo"),
                            rs.getString("Estacion")
                    )
            );
            
        }
        st.close();
        rs.close();
        return chequeos;
    }

        public ArrayList<Chequeo> getChequeos() throws SQLException {
        ArrayList<Chequeo> chequeos = new ArrayList<>();
        String sql = "SELECT Chequeo\n"
                + "      ,Factura\n"
                + "      ,UsuarioChequeo\n"
                + "      ,FechaChequeo\n"
                + "      ,Anulado\n"
                + "      ,RazonAnulado "
                + "      ,(SELECT CantChequeo FROM DM.[dbo].[CantidadDespachosPorFactura] CANT WHERE CANT.FACTURA=C.Factura) CantChequeo "
                + "      ,Estacion "
                + "  FROM DM.dbo.Chequeo C\n"
                + " WHERE\n"
                + "	C.FechaChequeo>=CAST(GETDATE() as Date)\n"
                + "ORDER BY\n"
                + "	FechaChequeo DESC";
        PreparedStatement st= this.con.prepareStatement(sql);
        ResultSet rs=st.executeQuery();
        while(rs.next()){
            chequeos.add(
                    new Chequeo(rs.getInt("Chequeo"),
                            rs.getString("Factura"),
                            rs.getString("UsuarioChequeo"),
                            rs.getTimestamp("FechaChequeo"),
                            0==rs.getInt("Anulado")?false:true,
                            rs.getString("RazonAnulado"),
                            rs.getInt("CantChequeo"),
                            rs.getString("Estacion")
                            
                    )
            );
            
        }
        st.close();
        rs.close();
        return chequeos;
    }
    public ArrayList<Chequeo> getChequeosFactura(String factura) throws SQLException {
        ArrayList<Chequeo> chequeos = new ArrayList<>();
        String sql = "SELECT Chequeo\n"
                + "      ,Factura\n"
                + "      ,UsuarioChequeo\n"
                + "      ,FechaChequeo\n"
                + "      ,Anulado\n"
                + "      ,RazonAnulado "
                + "      ,(SELECT COUNT(FACTURA) FROM [DM].[dbo].[Chequeo] CUENTA WHERE CUENTA.Factura=C.Factura) CantChequeo "
                + "      ,Estacion "
                + "  FROM DM.dbo.Chequeo C\n"
                + " WHERE\n"
                + "	C.Factura=? \n"
                + "ORDER BY\n"
                + "	FechaChequeo DESC";
        PreparedStatement st= this.con.prepareStatement(sql);
        st.setString(1, factura);
        ResultSet rs=st.executeQuery();
        while(rs.next()){
            chequeos.add(
                    new Chequeo(rs.getInt("Chequeo"),
                            rs.getString("Factura"),
                            rs.getString("UsuarioChequeo"),
                            rs.getTimestamp("FechaChequeo"),
                            0==rs.getInt("Anulado")?false:true,
                            rs.getString("RazonAnulado"),
                            rs.getInt("CantChequeo"),
                            rs.getString("Estacion")
                            
                    )
            );
            
        }
        st.close();
        rs.close();
        return chequeos;
    } 
      public ArrayList<Chequeo> getMasDeUnChequeo() throws SQLException {
        ArrayList<Chequeo> chequeos = new ArrayList<>();
        String sql = "SELECT TOP 500 Chequeo\n"
                + "      ,Factura\n"
                + "      ,UsuarioChequeo\n"
                + "      ,FechaChequeo\n"
                + "      ,Anulado\n"
                + "      ,RazonAnulado "
                + "      ,(SELECT COUNT(FACTURA) FROM [DM].[dbo].[Chequeo] CUENTA WHERE CUENTA.Factura=C.Factura) CantChequeo "
                + "      ,Estacion "
                + "  FROM DM.dbo.Chequeo C\n"
                + " WHERE\n"
                + "	(SELECT COUNT(FACTURA) FROM [DM].[dbo].[Chequeo] CUENTA WHERE CUENTA.Factura=C.Factura )>1 "
                + "ORDER BY\n"
                + "	FechaChequeo DESC";
        PreparedStatement st= this.con.prepareStatement(sql);
        ResultSet rs=st.executeQuery();
        while(rs.next()){
            chequeos.add(
                    new Chequeo(rs.getInt("Chequeo"),
                            rs.getString("Factura"),
                            rs.getString("UsuarioChequeo"),
                            rs.getTimestamp("FechaChequeo"),
                            0==rs.getInt("Anulado")?false:true,
                            rs.getString("RazonAnulado"),
                            rs.getInt("CantChequeo"),
                            rs.getString("Estacion")
                            
                    )
            );
            
        }
        st.close();
        rs.close();
        return chequeos;
    } 
}
