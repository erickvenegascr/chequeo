package com.almacen3r.chequeo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import com.almacen3r.chequeo.ArticuloVenta;

/**
 *
 * @author erick
 */
public class ArticuloVentaDAO {

    private Connection con;

    public ArticuloVentaDAO(Connection con) {
        this.con = con;
    }

    public ArrayList<ArticuloVenta> getVentas(String codArticulo) throws SQLException {
        ArrayList<ArticuloVenta> articuloVentas = new ArrayList<ArticuloVenta>();
        String sql = "declare @FechaInicial date\n"
                + "declare @FechaFinal date\n"
                + "declare @Art varchar(15)\n"
                + "set @FechaInicial = DATEADD(s,0,DATEADD(mm, DATEDIFF(m,0,Dateadd(yy,-1,Getdate())),0))\n"
                + "set @FechaFinal = GETDATE()\n"
                + "set @Art=? "
                + "SELECT\n"
                + "	Ventas.ARTICULO\n"
                + "	,SUM(Ventas.Cantidad) Cantidad\n"
                + "	,Tipo\n"
                + "	,Mes\n"
                + "	,Ano\n"
                + "FROM(\n"
                + "	SELECT \n"
                + "		FACTURA_LINEA.ARTICULO\n"
                + "		,FACTURA_LINEA.CANTIDAD*-1 Cantidad\n"
                + "		,([PRECIO_TOTAL]-[DESC_TOT_GENERAL])*-1 AS SubTotal,\n"
                + "		((([PRECIO_TOTAL]-[DESC_TOT_GENERAL])*(ROUND((ALMACEN_3R.FACTURA_LINEA.TOTAL_IMPUESTO1/([PRECIO_TOTAL]-[DESC_TOT_GENERAL]))*100,0)/100))+\n"
                + "		([PRECIO_TOTAL]-[DESC_TOT_GENERAL]))*-1 as Total,\n"
                + "		CAST(ALMACEN_3R.FACTURA.FECHA as Date) as Fecha,\n"
                + "		'Devolucion' as Tipo\n"
                + "		,FACTURA.VENDEDOR\n"
                + "		,DATEPART(MM,ALMACEN_3R.FACTURA.FECHA) Mes\n"
                + "		,DATEPART(YY,ALMACEN_3R.FACTURA.FECHA) Ano\n"
                + "	FROM \n"
                + "		ALMACEN_3R.FACTURA INNER JOIN ALMACEN_3R.FACTURA_LINEA \n"
                + "		ON ALMACEN_3R.FACTURA.TIPO_DOCUMENTO = ALMACEN_3R.FACTURA_LINEA.TIPO_DOCUMENTO AND ALMACEN_3R.FACTURA.FACTURA = ALMACEN_3R.FACTURA_LINEA.FACTURA\n"
                + "		INNER JOIN ALMACEN_3R.ARTICULO ON ALMACEN_3R.ARTICULO.ARTICULO=ALMACEN_3R.FACTURA_LINEA.ARTICULO and\n"
                + "		ALMACEN_3R.ALMACEN_3R.FACTURA_LINEA.TIPO_DOCUMENTO='D' AND \n"
                + "		ALMACEN_3R.FACTURA.ANULADA='N' AND \n"
                + "		ALMACEN_3R.FACTURA_LINEA.PRECIO_TOTAL>0 AND\n"
                + "		CAST(ALMACEN_3R.FACTURA.FECHA as Date)>=@FechaInicial AND\n"
                + "		CAST(ALMACEN_3R.FACTURA.FECHA as Date)<=@FechaFinal\n"
                + "		AND ALMACEN_3R.FACTURA_LINEA.ARTICULO=@Art\n"
                + "		UNION ALL\n"
                + "		SELECT  \n"
                + "				FACTURA_LINEA.ARTICULO\n"
                + "				,FACTURA_LINEA.CANTIDAD\n"
                + "				,[PRECIO_TOTAL]-[DESC_TOT_GENERAL] AS SubTotal,\n"
                + "				(([PRECIO_TOTAL]-[DESC_TOT_GENERAL])*(ROUND((ALMACEN_3R.FACTURA_LINEA.TOTAL_IMPUESTO1/([PRECIO_TOTAL]-[DESC_TOT_GENERAL]))*100,0)/100))+\n"
                + "				([PRECIO_TOTAL]-[DESC_TOT_GENERAL]) as Total,\n"
                + "				CAST(ALMACEN_3R.FACTURA.FECHA as Date) as Fecha,\n"
                + "				'Ventas' as Tipo\n"
                + "				,FACTURA.VENDEDOR\n"
                + "				,DATEPART(MM,ALMACEN_3R.FACTURA.FECHA) Mes\n"
                + "				,DATEPART(YY,ALMACEN_3R.FACTURA.FECHA) Ano\n"
                + "			FROM \n"
                + "				ALMACEN_3R.FACTURA INNER JOIN ALMACEN_3R.FACTURA_LINEA \n"
                + "						ON ALMACEN_3R.FACTURA.TIPO_DOCUMENTO = ALMACEN_3R.FACTURA_LINEA.TIPO_DOCUMENTO AND ALMACEN_3R.FACTURA.FACTURA = ALMACEN_3R.FACTURA_LINEA.FACTURA\n"
                + "						INNER JOIN ALMACEN_3R.ARTICULO ON ALMACEN_3R.ARTICULO.ARTICULO=ALMACEN_3R.FACTURA_LINEA.ARTICULO \n"
                + "			WHERE \n"
                + "				ALMACEN_3R.ALMACEN_3R.FACTURA_LINEA.TIPO_DOCUMENTO='F' AND \n"
                + "				ALMACEN_3R.FACTURA.ANULADA='N' AND \n"
                + "				ALMACEN_3R.FACTURA_LINEA.PRECIO_TOTAL>0 AND\n"
                + "				CAST(ALMACEN_3R.FACTURA.FECHA as Date)>=@FechaInicial AND\n"
                + "				CAST(ALMACEN_3R.FACTURA.FECHA as Date)<=@FechaFinal\n"
                + "				AND ALMACEN_3R.FACTURA_LINEA.ARTICULO=@Art\n"
                + "	)Ventas\n"
                + "GROUP BY\n"
                + "	VENTAS.ARTICULO\n"
                + "	,Ventas.Tipo\n"
                + "	,Ventas.Mes\n"
                + "	,Ventas.Ano\n"
                + "ORDER BY\n"
                + "	ARTICULO\n"
                + "	,Ano DESC\n"
                + "	,MES DESC";
        PreparedStatement st = con.prepareStatement(sql);
        st.setString(1, codArticulo);
        ResultSet rs = st.executeQuery();
        while (rs.next()) {
            articuloVentas.add(
                    new ArticuloVenta(rs.getString("ARTICULO"),
                            rs.getDouble("Cantidad"),
                            rs.getString("Tipo"),
                            rs.getInt("mes"),
                            rs.getInt("ano")
                    )
            );

        }
        st.close();
        rs.close();
        return articuloVentas;
    }
}
