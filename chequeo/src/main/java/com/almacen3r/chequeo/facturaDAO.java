package com.almacen3r.chequeo;



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.almacen3r.chequeo.articulo;
import com.almacen3r.chequeo.Factura;
import com.almacen3r.chequeo.conexion;

/**
 *
 * @author erick
 */
public class facturaDAO {


    private Connection con;
    private ResultSet factResult;
    private ResultSet artResult;
    private Factura factura;
    private PreparedStatement factSQL;
    private PreparedStatement artSQL;
    private Statement SQL;

    public facturaDAO(Connection con) {
        this.con=con;
    }

    public Factura getFactura(String numFactura) {
        this.factura = new Factura();
        ArrayList<articulo> artList = new ArrayList<articulo>();
        try {
            this.factSQL = this.con.prepareStatement("SELECT\n"
                    + "F.FACTURA\n"
                    + ",F.FECHA_HORA FECHA\n"
                    + ",F.CLIENTE\n"
                    + ",F.NOMBRE_CLIENTE\n"
                    + ",V.NOMBRE VENDEDOR\n"
                    + ",F.TOTAL_FACTURA\n"
                    + ",F.ANULADA "
                    + "FROM "
                    + "ALMACEN_3R.FACTURA F INNER JOIN ALMACEN_3R.VENDEDOR V ON F.VENDEDOR=V.VENDEDOR "
                    + "WHERE F.FACTURA = ?");
            this.factSQL.setString(1, numFactura);
            this.factResult = this.factSQL.executeQuery();
            while (this.factResult.next()) {
                this.factura.setFactura(this.factResult.getString("Factura"));
                this.factura.setFecha(this.factResult.getTimestamp("Fecha"));
                this.factura.setCliente(this.factResult.getString("CLIENTE"));
                this.factura.setClienteNombre(this.factResult.getString("NOMBRE_CLIENTE"));
                this.factura.setVendedor(this.factResult.getString("VENDEDOR"));
                this.factura.setMontoTotal(this.factResult.getDouble("TOTAL_FACTURA"));
                if (this.factResult.getString("ANULADA").equals("S")) {
                    this.factura.setAnulada(true);
                } else {
                    this.factura.setAnulada(false);
                }
            }
            this.factResult.close();
            this.factSQL.close();

            this.artSQL = this.con.prepareStatement("SELECT\n"
                    + "	FL.ARTICULO\n"
                    + "	,FL.LINEA\n"
                    + "	,CASE\n"
                    + "		WHEN LEN(CAST(FL.DESCRIPCION AS NVARCHAR(50)))>0 THEN FL.DESCRIPCION\n"
                    + "		ELSE A.DESCRIPCION\n"
                    + "	END Descripcion\n"
                    + "	,FL.CANTIDAD\n"
                    + "	,FL.FACTURA\n"
                    + "FROM\n"
                    + "	ALMACEN_3R.FACTURA_LINEA FL INNER JOIN ALMACEN_3R.ARTICULO A\n"
                    + "	ON FL.ARTICULO=A.ARTICULO\n"
                    + "WHERE\n"
                    + "	FL.FACTURA=? "
                    + " ORDER BY "
                    + "  A.CLASIFICACION_1 DESC "
                    + "  ,A.DESCRIPCION ");
            this.artSQL.setString(1, numFactura);
            this.artResult = this.artSQL.executeQuery();
            while (this.artResult.next()) {
                articulo a = new articulo(
                        this.artResult.getString("ARTICULO"),
                        this.artResult.getInt("LINEA"),
                        this.artResult.getString("Descripcion"),
                        this.artResult.getDouble("CANTIDAD"),
                        this.artResult.getString("FACTURA")
                );
                artList.add(a);
            }
            this.factura.setArticulos(artList);
            this.artResult.close();
            this.artSQL.close();
        } catch (SQLException ex) {
            Logger.getLogger(facturaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return this.factura;
    }
}
