package com.almacen3r.chequeo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author erick
 */
public class Factura {

    private String factura;
    private Date fecha;
    private String cliente;
    private String clienteNombre;
    private ArrayList<articulo> articulos;
    private String vendedor;
    private double montoTotal;
    private boolean anulada;

    public String getFactura() {
        return factura;
    }

    public void setFactura(String factura) {
        this.factura = factura;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getClienteNombre() {
        return clienteNombre;
    }

    public void setClienteNombre(String clienteNombre) {
        this.clienteNombre = clienteNombre;
    }

   public ArrayList<articulo> getArticulos() {
        return articulos;
    }

    public void setArticulos(ArrayList<articulo> articulos) {
        this.articulos=new ArrayList<articulo>();
        this.articulos.addAll(articulos);
    }

    public String getVendedor() {
        return vendedor;
    }

    public void setVendedor(String vendedor) {
        this.vendedor = vendedor;
    }

    public double getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(double montoTotal) {
        this.montoTotal = montoTotal;
    }

    public boolean isAnulada() {
        return anulada;
    }

    public void setAnulada(boolean anulada) {
        this.anulada = anulada;
    }

    @Override
    public String toString() {
        return "facturaVO{" + "factura=" + factura + ", fecha=" + fecha + ", cliente=" + cliente + ", clienteNombre=" + clienteNombre + ", [articulos=" + articulos.toString() + "], vendedor=" + vendedor + ", montoTotal=" + montoTotal + ", anulada=" + anulada + '}';
    }
}
