package com.almacen3r.chequeo;



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import com.almacen3r.chequeo.ArticuloCompra;

public class ArticuloCompraDAO {

    private Connection con;

    public ArticuloCompraDAO(Connection con) {
        this.con = con;
    }

    public ArrayList<ArticuloCompra> getCompras(String codArticulo) throws SQLException {
        ArrayList<ArticuloCompra> articuloCompras = new ArrayList<ArticuloCompra>();
        String sql = "SELECT\n"
                + "	ROUND(EL.COST_UN_REAL_LOCAL,0) PrecioUnit	\n"
                + "	,EL.PROVEEDOR Proveedor\n"
                + "	,EL.ARTICULO\n"
                + "	,P.NOMBRE\n"
                + "	,SUM(EL.CANTIDAD_RECIBIDA) Comprado\n"
                + "	,DATEPART(MM,E.FECHA_EMBARQUE) Mes\n"
                + "	,DATEPART(YY,E.FECHA_EMBARQUE) Ano\n"
                + "	FROM\n"
                + "	ALMACEN_3R.EMBARQUE_LINEA EL INNER JOIN ALMACEN_3R.EMBARQUE E\n"
                + "	ON EL.EMBARQUE=E.EMBARQUE INNER JOIN ALMACEN_3R.PROVEEDOR P \n"
                + "	ON EL.PROVEEDOR=P.PROVEEDOR\n"
                + "WHERE\n"
                + "    EL.ARTICULO =? "
                + "GROUP BY\n"
                + "	ROUND(EL.COST_UN_REAL_LOCAL,0)	\n"
                + "	,EL.PROVEEDOR\n"
                + "	,EL.ARTICULO\n"
                + "	,P.NOMBRE\n"
                + "	,DATEPART(MM,E.FECHA_EMBARQUE) \n"
                + "	,DATEPART(YY,E.FECHA_EMBARQUE) \n"
                + "ORDER BY \n"
                + "	Articulo\n"
                + "	,Ano desc\n"
                + "	,Mes desc";
        PreparedStatement st = con.prepareStatement(sql);
        st.setString(1, codArticulo);
        ResultSet rs = st.executeQuery();
        while (rs.next()) {
            articuloCompras.add(
                    new ArticuloCompra(rs.getString("Proveedor"),
                            rs.getString("NOMBRE"),
                            rs.getString("ARTICULO"),
                            rs.getInt("Mes"),
                            rs.getInt("Ano"),
                            rs.getDouble("Comprado"),
                            rs.getDouble("PrecioUnit")
                    )
            );
        }
        rs.close();
        st.close();
        return articuloCompras;
    }
}
