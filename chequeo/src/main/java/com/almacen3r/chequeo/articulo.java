package com.almacen3r.chequeo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author erick
 * Clase base para los articulos
 */
public class articulo {
    private String articulo;
    private int orden;
    private String nombre;
    private double cantidad;
    private String factura;
    
    

    public articulo(String art, int orden, String nomb, double cant, String factura){
        this.articulo=art;
        this.orden=orden;
        this.nombre=nomb;
        this.cantidad=cant;
        this.factura=factura;  
    }

    public articulo() {
    }
    
    public String getArticulo() {
        return articulo;
    }
    
    public void setArticulo(String articulo) {
        this.articulo = articulo;
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public String getFactura() {
        return factura;
    }

    public void setFactura(String factura) {
        this.factura = factura;
    }

    @Override
    public String toString() {
        return "articuloVO{" + "articulo=" + articulo + ", orden=" + orden + ", nombre=" + nombre + ", cantidad=" + cantidad + ", factura=" + factura + '}';
    }

}
