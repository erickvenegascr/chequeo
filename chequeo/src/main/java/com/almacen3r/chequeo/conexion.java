package com.almacen3r.chequeo;



import java.sql.DriverManager;
/*
 import java.sql.Statement;
 import java.sql.ResultSet;
 import java.sql.ResultSetMetaData;
 */
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


public class conexion {

    
    private String Servidor;
    private final String BaseDatos;
    private final String Usuario;
    private final String Clave;
    
    
    
    private Connection Conexion;
    /*
     private Statement Consulta;
     private ResultSet ResultadoConsulta;
     private ResultSetMetaData MetaDataResultadoConsulta;
     private Object[][] Resultado;
     */

    public conexion() {
        this.Servidor="192.168.0.21";
        this.BaseDatos="ALMACEN_3R";
        this.Usuario="sa";
        this.Clave="pwdSA3R";
        //this.Clave=main.getClave();
        //this.Usuario=main.getUsuario();
        /*
        if(main.getUsuario().contentEquals("SA")){
            this.Usuario=main.getUsuario();
        }else{
            this.Usuario="X"+main.getUsuario();
        }
                */
        try {
            DriverManager.registerDriver(new com.microsoft.sqlserver.jdbc.SQLServerDriver());
            Conexion = DriverManager.getConnection("jdbc:sqlserver://" + Servidor + ";databaseName=" + BaseDatos + ";user=" + Usuario + ";password=" + Clave + ";");
            //Conexion.setAutoCommit(false);
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "PROBLEMA EN LA CONEXIÓN");
        }
    }

    public Connection getConexion() {
        return Conexion;
    }

    public void Desconectar() {
        try {
            Conexion.close();
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
