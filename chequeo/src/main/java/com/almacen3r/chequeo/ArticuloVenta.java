package com.almacen3r.chequeo;



public class ArticuloVenta {

    private String articulo;
    private double cantidad;
    private String tipo;
    private int mes;
    private int ano;

    public ArticuloVenta(String articulo, double cantidad, String tipo, int mes, int ano) {
        this.articulo = articulo;
        this.cantidad = cantidad;
        this.tipo = tipo;
        this.mes = mes;
        this.ano = ano;
    }

    public String getArticulo() {
        return articulo;
    }

    public void setArticulo(String articulo) {
        this.articulo = articulo;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    @Override
    public String toString() {
        return "ArticuloVenta{" + "articulo=" + articulo + ", cantidad=" + cantidad + ", tipo=" + tipo + ", mes=" + mes + ", ano=" + ano + '}';
    }

}
