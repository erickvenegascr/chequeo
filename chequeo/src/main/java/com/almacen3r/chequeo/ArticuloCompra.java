package com.almacen3r.chequeo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author erick
 */
public class ArticuloCompra {
    private String codProveedor;
    private String nombProveedor;
    private String codArticulo;
    private int mes;
    private int ano;
    private double cantidad;
    private double costoUnit;
    
    public ArticuloCompra(String codProveedor, String nombProveedor, String codArticulo, int mes, int ano, double cantidad,double costoUnit){
        this.codProveedor=codProveedor;
        this.nombProveedor=nombProveedor;
        this.codArticulo=codArticulo;
        this.mes=mes;
        this.ano=ano;
        this.cantidad=cantidad;
        this.costoUnit=costoUnit;
    }

    public String getCodProveedor() {
        return codProveedor;
    }

    public void setCodProveedor(String codProveedor) {
        this.codProveedor = codProveedor;
    }

    public String getNombProveedor() {
        return nombProveedor;
    }

    public void setNombProveedor(String nombProveedor) {
        this.nombProveedor = nombProveedor;
    }

    public String getCodArticulo() {
        return codArticulo;
    }

    public void setCodArticulo(String codArticulo) {
        this.codArticulo = codArticulo;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public double getCostoUnit() {
        return costoUnit;
    }

    public void setCostoUnit(double costoUnit) {
        this.costoUnit = costoUnit;
    }

    @Override
    public String toString() {
        return "ArticuloCompra{" + "codProveedor=" + codProveedor + ", nombProveedor=" + nombProveedor + ", codArticulo=" + codArticulo + ", mes=" + mes + ", ano=" + ano + ", cantidad=" + cantidad + ", costoUnit=" + costoUnit + '}';
    }
    
}
