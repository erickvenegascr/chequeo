

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import com.almacen3r.chequeo.ArticuloCompraDAO;
import com.almacen3r.chequeo.ArticuloVentaDAO;
import com.almacen3r.chequeo.ChequeoDAO;
import com.almacen3r.chequeo.ArticuloCompra;
import com.almacen3r.chequeo.ArticuloVenta;
import com.almacen3r.chequeo.Chequeo;
import com.almacen3r.chequeo.Factura;
import com.almacen3r.chequeo.facturaDAO;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;

/**
 *
 * @author erick
 */
public class controlador extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     *
     */
    private DataSource ds;
    private Connection con;

    private static final long serialVersionUID = 1L;
    private static final org.apache.log4j.Logger log = LogManager.getLogger("Serverlet Chequeo : ");

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // 
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String accion = request.getParameter("accion");

        if (accion != null) {

            if (accion.contains("consultarDetalleFactura")) {
                String numFactura = request.getParameter("factura");
                try {
                    this.con=ds.getConnection();
                } catch (SQLException ex) {
                    Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
                }
                Factura factura = new facturaDAO(this.con).getFactura(numFactura);
                try {
                    this.con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
                }
                request.setAttribute("factura", factura);
                getServletContext().getRequestDispatcher("/jsp/facturaDetalle.jsp").forward(request, response);
            }
            if(accion.contains("chequeosfactura")){
                try {
                    String numFactura=request.getParameter("factura");
                    ArrayList<Chequeo> chequeos = new ArrayList<Chequeo>();
                    this.con = ds.getConnection();
                    chequeos.addAll(new ChequeoDAO(this.con).getChequeosFactura(numFactura));
                    this.con.close();
                    request.setAttribute("chequeos", chequeos);
                    getServletContext().getRequestDispatcher("/jsp/chequeos.jsp").forward(request, response);
                } catch (SQLException ex) {
                    Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if(accion.contains("masDeUnaChequeada")){
                try {
                    this.con = ds.getConnection();
                    ArrayList<Chequeo> chequeos = new ArrayList<Chequeo>();
                    chequeos.addAll(new ChequeoDAO(this.con).getMasDeUnChequeo());
                    this.con.close();
                    request.setAttribute("chequeos", chequeos);
                    getServletContext().getRequestDispatcher("/jsp/chequeos.jsp").forward(request, response);
                } catch (SQLException ex) {
                    Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        } else {

            try {              
                con = ds.getConnection();
                ArrayList<Chequeo> chequeos = new ArrayList<>();
                chequeos.addAll(new ChequeoDAO(this.con).getChequeos());
                this.con.close();
                request.setAttribute("chequeos", chequeos);
                getServletContext().getRequestDispatcher("/jsp/chequeos.jsp").forward(request, response);
                 
            } catch (SQLException ex) {
                Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public void init(ServletConfig config) {
     
         try {
            super.init(config);
            InitialContext initContext = new InitialContext();
            Context env = (Context) initContext.lookup("java:comp/env");
            ds = (DataSource) env.lookup("jdbc/almacen3r");
            BasicConfigurator.configure();
        } catch (ServletException | NamingException ex) {
            Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
